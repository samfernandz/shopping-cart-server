const mongoose = require('mongoose');
const Joi = require('joi');

const Customer = mongoose.model('Customer', new mongoose.Schema({
  customer_id: {
    type: String,
    required: true
  },
  name : {
    type: String,
    required: true
  },
  email : {
    type: String,
  },
  contact_number : {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  }
}, { timestamps: { createdAt: 'customer_created' } }));

function validateCustomer(customer) {
  const schema = {
    customerId: Joi.string().required(),
    name: Joi.string().required(),
    email: Joi.string().required(),
    contactNumber: Joi.string().required(),
    address: Joi.string()
  }
  return Joi.validate(customer, schema);
}

exports.Customer = Customer;
exports.validate = validateCustomer;