const mongoose = require('mongoose');
const Joi = require('joi');

const OrderItem = mongoose.model('OrderItem', new mongoose.Schema({
  item_id : {
    type: String,
    required: true
  },
  name : {
    type: String,
    required: true
  },
  unit_price : {
    type: Number,
    required: true
  },
  quantity : {
    type: Number,
    required: true
  },
  total: {
    type: Number,
    required: true
  }
}, { timestamps: { createdAt: 'orderitem_created' } }));

function validateOrderItem(orderItem) {
  const schema = {
    itemId: Joi.string().required(),
    name: Joi.string().required(),
    unitPrice: Joi.required(),
    quantity: Joi.required(),
    total: Joi.required()
  }
  return Joi.validate(orderItem, schema);
}

exports.OrderItem = OrderItem;
exports.validate = validateOrderItem;