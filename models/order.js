const mongoose = require('mongoose');
const Joi = require('joi');

const Order = mongoose.model('Order', new mongoose.Schema({
  order_id : {
    type: String,
    required: true
  },
  order_items: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'OrderItem'
  }],
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer'
  },
  shipping_address : {
    type: String,
    required: true
  },
  grand_total : {
    type: Number,
    required: true
  },
  invoice: {
    type: String,
    required: true
  }
}, { timestamps: { createdAt: 'order_date' } }));

function validateOrder(order) {
  const schema = {
    orderId: Joi.string().required(),
    orderItems: Joi.array().required(),
    customerId: Joi.string().required(),
    shippingAddress: Joi.string().required(),
    grandTotal: Joi.required(),
    invoice: Joi.string().required(),
  }
  return Joi.validate(order, schema);
}

exports.Order = Order;
exports.validate = validateOrder;