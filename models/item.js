const mongoose = require('mongoose');
const Joi = require('joi');

const Item = mongoose.model('Item', new mongoose.Schema({
  item_id : {
    type: String,
    required: true
  },
  name : {
    type: String,
    required: true
  },
  unit_price : {
    type: Number,
    required: true
  },
  quantity : {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  }
}, { timestamps: { createdAt: 'item_created' } }));

function validateItem(item) {
  const schema = {
    itemId: Joi.string().required(),
    name: Joi.string().required(),
    unitPrice: Joi.required(),
    quantity: Joi.required(),
    description: Joi.string()
  }
  return Joi.validate(item, schema);
}

exports.Item = Item;
exports.validate = validateItem;