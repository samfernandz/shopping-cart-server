const mongoose = require('mongoose');
const Joi = require('joi');

const Invoice = mongoose.model('Invoice', new mongoose.Schema({
  path : {
    type: String,
    required: true
  },
  file_name : {
    type: String,
    required: true
  },
}, { timestamps: { createdAt: 'invoice_created' } }));

exports.Invoice = Invoice;