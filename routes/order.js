const express = require('express');
const router = express.Router();

const { Order, validate }  = require('../models/order');

router.get('/', async (req, res) => {
  const orders = await Order.find();
  res.send(orders);
});

router.get('/last', async (req, res) => {
  try {
    const order = await Order.findOne()
      .sort({ _id: -1 })
      .select({ order_id: 1 })
      .limit(1);

    if(!order) {
      return res.send({ order_id: 'ORD1' });
    }

    return res.send(order);
  } catch (error) {
    return res.status(400).send(error.message);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const order = await Order.findById(req.params.id)
    if (!order) return res.status(404).send('The order with the given ID was not found.');
    return res.send(order);
  } catch (error) {
    return res.status(400).send(error.message);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const order = await Order.findByIdAndRemove( req.params.id );
    if (!order) return res.status(404).send('The order with the given ID was not found.');
    return res.send(order);
  } catch (error) {
    return res.status(400).send(error.message);
  }
});

router.post('/', async (req, res) => {

  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  try {
    let order = new Order({
      order_id: req.body.orderId,
      order_items: req.body.orderItems,
      customer: req.body.customerId,
      shipping_address: req.body.shippingAddress,
      grand_total: req.body.grandTotal,
      invoice: req.body.invoice
    })

    order = await order.save();
    return res.status(201).send(order);

  } catch (error) {
    console.error('error', error);
    return res.status(400).send(error.message);
  }

});

module.exports = router;