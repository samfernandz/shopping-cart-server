const express = require('express');
const router = express.Router();

const { OrderItem, validate }  = require('../models/order-item');

router.get('/', async (req, res) => {
  const orderItems = await OrderItem.find();
  res.send(orderItems);
});


router.get('/:id', async (req, res) => {
  try {

    const item = await OrderItem.findById(req.params.id)
    if (!item) return res.status(404).send('The order item with the given ID was not found.');
    return res.send(item);

  } catch (error) {
    return res.status(400).send(error.message);
  }
});

router.post('/', async (req, res) => {

  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  try {
    let item = new OrderItem({
      item_id: req.body.itemId,
      name: req.body.name,
      unit_price: req.body.unitPrice,
      quantity: req.body.quantity,
      total: req.body.total,
    })

    item = await item.save();
    return res.status(201).send({ status: 'created', _id: item._id});

  } catch (error) {
    console.error('error', error);
    return res.status(400).send(error.message);
  }

});

module.exports = router;