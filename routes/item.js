const express = require('express');
const router = express.Router();

const { Item, validate }  = require('../models/item');

router.get('/', async (req, res) => {
  const items = await Item.find();
  res.send(items);
});

router.get('/last', async (req, res) => {
  try {
    const item = await Item.findOne()
      .sort({ _id: -1 })
      .select({ item_id: 1 })
      .limit(1);

    if(!item) {
      return res.send({ item_id: 'ITM1' });
    }

    return res.send(item);
  } catch (error) {
    return res.status(400).send(error.message);
  }
});

router.get('/:id', async (req, res) => {
  try {

    const item = await Item.findById(req.params.id)
    if (!item) return res.status(404).send('The item with the given ID was not found.');
    return res.send(item);

  } catch (error) {
    return res.status(400).send(error.message);
  }
});

router.post('/', async (req, res) => {

  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  try {
    let item = new Item({
      item_id: req.body.itemId,
      name: req.body.name,
      unit_price: req.body.unitPrice,
      quantity: req.body.quantity,
      description: req.body.description,
    })

    item = await item.save();
    return res.status(201).send(item);

  } catch (error) {
    console.error('error', error);
    return res.status(400).send(error.message);
  }

});

module.exports = router;