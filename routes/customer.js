const express = require('express');
const router = express.Router();

const { Customer, validate }  = require('../models/customer');

router.get('/', async (req, res) => {
  const customers = await Customer.find();
  res.send(customers);
});

router.get('/last', async (req, res) => {
  try {
    const customer = await Customer.findOne()
      .sort({ _id: -1 })
      .select({ customer_id: 1 })
      .limit(1);

    if(!customer) {
      return res.send({ customer_id: 'CUS1' });
    }

    return res.send(customer);
  } catch (error) {
    return res.status(400).send(error.message);
  }
});


router.get('/find/:key', async (req, res) => {
  try {
    const customers = await Customer
      .find()
      .or([
        { customer_id: new RegExp(`^${req.params.key}`, "i") },
        { name: new RegExp(`.*${req.params.key}.*`, "i") }
      ])
      .select({ customer_id: 1, name: 1 });

    if (!customers) return res.status(404).send('Customers not found.');
    return res.send(customers);

  } catch (error) {
    return res.status(400).send(error.message);
  }
});

router.post('/', async (req, res) => {

  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  try {
    let customer = new Customer({
      customer_id: req.body.customerId,
      name: req.body.name,
      email: req.body.email,
      contact_number: req.body.contactNumber,
      address: req.body.address,
    })

    customer = await customer.save();
    return res.status(201).send(customer);

  } catch (error) {
    console.error('error', error);
    return res.status(400).send(error.message);
  }

});

module.exports = router;