const express = require('express');
const router = express.Router();
const { Invoice }  = require('../models/invoice');

const upload = require('../startup/config');

router.get('/', async (req, res) => {
  const invoices = await Invoice.find();
  res.send(invoices);
});

router.post('/', upload.single('invoice'), (req, res, next) => {
  next();
});

router.post('/show', (req, res) => {
  res.sendFile(req.body.path , { root : './'});
});

router.post('/', async (req, res) => {
  // console.log('req.file', req.file);
  try {
    let invoice = new Invoice({
      file_name: req.file.filename,
      path: req.file.path
    });

    invoice = await invoice.save();
    return res.status(201).send(invoice);

  } catch (error) {
    console.error('error', error);
    return res.status(400).send(error.message);
  }
});

module.exports = router;