const logger = require('../log/logger');

// handle request processing errors
module.exports = function(err, req, res, next){
  // Log the exception
  logger.error(err.message);
  res.status(500).send('Something failed.');
}