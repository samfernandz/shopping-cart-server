const express = require('express');
const error = require('../middleware/server-error');

const orderRoute = require('../routes/order');
const itemRoute = require('../routes/item');
const customerRoute = require('../routes/customer');
const orderItemRoute = require('../routes/order-item');
const invoicesRoute = require('../routes/invoices');

module.exports = function (app) {
  app.use(express.json());
  app.use(express.static('public'))
  app.use('/api/orders', orderRoute);
  app.use('/api/items', itemRoute);
  app.use('/api/customers', customerRoute);
  app.use('/api/order-items', orderItemRoute);
  app.use('/api/invoices', invoicesRoute);
  app.use(error);
}