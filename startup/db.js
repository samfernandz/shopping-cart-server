const logger = require('../log/logger');
const mongoose = require('mongoose');
const config = require('config');

module.exports = function () {
  const host = config.get('db.host');
  mongoose.connect(host)
    .then(() => logger.info(`connected to ${host}`));
}