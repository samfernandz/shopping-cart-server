const multer = require('multer');
const crypto = require('crypto');
const mime = require('mime');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/invoices')
  },
  filename: (req, file, cb) => {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, file.fieldname + '-' + Date.now() + '.' + mime.getExtension(file.mimetype));
    });
  }
});

module.exports = multer({storage: storage});