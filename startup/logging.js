const logger = require('../log/logger');

module.exports = function () {
  // Uncaught Exception handling
  process.on('uncaughtException', ex => {
    logger.error(ex.stack);
    // process.exit(1);
  });

  // unhandled rejections (promisses)
  process.on('unhandledRejection', ex => {
    logger.error(ex.stack);
    // process.exit(1);
  });
}