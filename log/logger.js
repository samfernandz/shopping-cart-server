const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;
const path = require('path');

const logFormat = printf(info => {
  return `${info.timestamp} -${info.level}-: ${info.message}`;
});

const logger = createLogger({
  format: combine(
    timestamp(),
    logFormat
  ),
  transports: [
    new transports.Console({ colorize: true, prettyPrint: true }),
  ]
});

if (process.env.NODE_ENV === 'production') {
  logger.add(new transports.File({ filename: path.resolve(__dirname, 'server.log') }));
}

module.exports = logger;